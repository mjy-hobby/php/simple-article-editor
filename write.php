<?php
error_reporting(0);
if($_SERVER['REQUEST_METHOD'] !== 'POST')
{
    ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
    $conn->close();
    exit();
}
include ".htdbconfig.php";
if(!isset($_POST['title'], $_POST['img_count'], $_POST['text0']))
{
    ?><script>alert("뭔가 잘못됐는데?"); history.back();</script><?php
    $conn->close();
    exit();
}
$uploads_dir = './images/';
$allowed_ext = array('jpg','jpeg','png','gif');
$img_count = intval($_POST['img_count']);
$stmt = $conn->prepare("INSERT INTO articles (title, msg) VALUES (?, ?)");
if(!$stmt)
{
    ?><script>alert("서버 오류. 관리자에게 문의 바랍니다."); history.back();</script><?php
    $conn->close();
    exit();
}
$stmt->bind_param("ss", $_POST['title'], $_POST['text0']);
if(!$stmt->execute())
{
    ?><script>alert("글쓰기를 실패했습니다."); history.back();</script><?php
    $stmt->close();
    $conn->close();
    exit();
}
$id = $stmt->insert_id;
$stmt->close();
$stmt = $conn->prepare("INSERT INTO articles_part (seq, n, img, msg) VALUES ($id, ?, ?, ?)");
for($i = 0; $i < $img_count;)
{
    $i++;
    if(!isset($_POST['text' . $i], $_FILES['img' . $i]))
    {
        ?><script>alert("뭔가 잘못됐는데?"); history.back();</script><?php
        $stmt->close();
        $conn->close();
        exit();
    }
    $error = $_FILES['img' . $i]['error'];
    if($error != UPLOAD_ERR_OK)
    {
        switch($error)
        {
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            ?><script>alert("파일이 너무 큽니다. (<?=addslashes($error)?>)"); history.back();</script><?php
            break;
        case UPLOAD_ERR_NO_FILE:
            ?><script>alert("파일이 첨부되지 않았습니다. (<?=addslashes($error)?>)"); history.back();</script><?php
            break;
        default:
            ?><script>alert("파일이 제대로 업로드되지 않았습니다. (<?=addslashes($error)?>)"); history.back();</script><?php
        }
        $stmt->close();
        $conn->close();
        exit();
    }
    $ext = explode('.', $_FILES['img' . $i]['name']);
    $ext = strtolower(array_pop($ext));
    if(!in_array($ext, $allowed_ext))
    {
        ?><script>alert("허용되지 않는 확장자입니다. (<?=addslashes($ext)?>)"); history.back();</script><?php
        $stmt->close();
        $conn->close();
        exit();
    }
    if(!getimagesize($_FILES['img' . $i]['tmp_name'])) // TODO: Validate image another way
    {
        ?><script>alert("올바른 이미지 파일이 아닙니다. (<?=addslashes($_FILES['img' . $i]['name'])?>)"); history.back();</script><?php
        $stmt->close();
        $conn->close();
        exit();
    }
    if($_FILES['img' . $i]['size'] > 3145728) // 3MB
    {
        ?><script>alert("파일 크기가 너무 큽니다. (<?=addslashes($_FILES['img' . $i]['name'])?>)"); history.back();</script><?php
        $stmt->close();
        $conn->close();
        exit();
    }
    $img_path = date('YmdHis') . "-$id-$i.$ext";
    $stmt->bind_param("iss", $i, $img_path, $_POST['text' . $i]);
    if(!$stmt->execute())
    {
        ?><script>alert("업로드를 실패했습니다."); history.back();</script><?php
        $stmt->close();
        $conn->close();
        exit();
    }
    if(!file_exists($uploads_dir))
    {
        mkdir($uploads_dir);
    }
    if(!move_uploaded_file($_FILES['img' . $i]['tmp_name'], $uploads_dir . $img_path))
    {
        ?><script>alert("이미지 업로드를 실패했습니다. (<?=addslashes($_FILES['img' . $i]['name'])?>)"); history.back();</script><?php
        $stmt->close();
        $conn->close();
        exit();
    }
}
$stmt->close();
$conn->close();
header("Location: list.php");
?>