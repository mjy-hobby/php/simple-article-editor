<?php
if(!isset($_GET['seq']))
{
    ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
    exit();
}
$seq = intval($_GET['seq']);
if(!$seq)
{
    ?><script>alert("잘못된 글 번호입니다."); history.back();</script><?php
    exit();
}
include ".htdbconfig.php";
if(!($result = $conn->query("SELECT title, msg FROM articles WHERE seq = $seq")))
{
    ?><script>alert("글 조회 실패"); history.back();</script><?php
    $conn->close();
    exit();
}
if(!($row = $result->fetch_assoc()))
{
    ?><script>alert("글 없음"); history.back();</script><?php
    $conn->close();
    exit();
}
$result->close();
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title><?=htmlspecialchars($row['title'])?></title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <h1><?=htmlspecialchars($row['title'])?></h1>
        <hr />
        <p class="p-w"><?=nl2br(htmlspecialchars($row['msg']))?></p><?php
if(!($result = $conn->query("SELECT img, msg FROM articles_part WHERE seq = $seq ORDER BY n")))
{
    echo '
        <span>글 불러오기를 실패했습니다.</span>';
}
else
{
    $uploads_dir = './images/';
    while($row = $result->fetch_assoc())
    {
        echo "
        <img src=\"$uploads_dir$row[img]\" />
        <p class=\"p-w\">" . nl2br(htmlspecialchars($row['msg'])) . '</p>';
    }
    $result->close();
}
$conn->close();
?>

    <a href="list.php">글 목록</a>
    </body>
</html>