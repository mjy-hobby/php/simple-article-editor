<?php
include ".htdbconfig.php";
$page_size = 10;
$page_index = 0;
if(isset($_GET['s']))
{
    $tmp = intval($_GET['s']);
    if($tmp) $page_size = $tmp;
}
if(isset($_GET['i']))
{
    $page_index = intval($_GET['i']) - 1;
}
$get_i = $page_index + 1;
$page_index *= $page_size;
if(!($result = $conn->query("SELECT seq, title FROM articles ORDER BY seq DESC LIMIT $page_index, $page_size")))
{
    ?><script>alert("목록 조회 실패"); history.back();</script><?php
    $conn->close();
    exit();
}
if(!($row = $result->fetch_assoc()))
{
    ?><script>alert("글 없음"); history.back();</script><?php
    $conn->close();
    exit();
}
?><!DOCTYPE HTML>
    <head>
        <meta charset="UTF-8" />
        <title>Simple Article Editor</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>번호</th>
                    <th>제목</th>
                </tr>
            </thead>
            <tbody><?php
do
{
    echo "
                <tr>
                    <td>$row[seq]</td>
                    <td><a href=\"view.php?seq=$row[seq]\">" . htmlspecialchars($row['title']) . "</a></td>
                </tr>";
}
while($row = $result->fetch_assoc());
$result->free();
?>

            </tbody>
        </table>
        <p><?php
if(!($count = $conn->query("SELECT COUNT(*) FROM articles")))
{
    echo '
            개수를 로드하지 못했습니다.';
}
else
{
    $count = $count->fetch_array()[0]; // 여기에 예외 처리가 필요한가요?
    $page_count = floor($count / $page_size);
    $page_start = floor($page_index / 10 / $page_size) * 10;
    $tmp = $page_size != 10 ? "s=$page_size&i=" : 'i=';
    if($page_start)
    {
        echo "
            <a href=\"list.php?$tmp$page_start\">이전</a>";
    }
    for($i = 0; $i < 10;)
    {
        $i++;
        $temp = $page_start + $i;
        if($temp > floor(($count - 1) / $page_size) + 1) break;
        echo $get_i == $temp ? "
            <b>$temp</b>" : "
            <a href=\"list.php?$tmp$temp\">$temp</a>";
    }
    if(($page_start + $page_size) * 10 < $count)
    {
        $temp = $page_start + 11;
        echo "
            <a href=\"list.php?$tmp$temp\">다음</a>";
    }
}
?>

        </p>
    </body>
</html><?php
$conn->close();
?>